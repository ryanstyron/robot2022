// Copyright (c) OpenRobotGroup.
// Open Source Software; you can modify and/or share it under the terms of
// the BSD license file in the root directory of this project.

// Copyright (c) FIRST and other WPILib contributors.
// Open Source Software; you can modify and/or share it under the terms of
// the WPILib BSD license file in the root directory of this project.

package frc.robot;

import edu.wpi.first.math.util.Units;

/**
 * The Constants class provides a convenient place for teams to hold robot-wide numerical or boolean
 * constants. This class should not be used for any other purpose. All constants should be declared
 * globally (i.e. public static). Do not put anything functional in this class.
 *
 * <p>It is advised to statically import this class (or one of its inner classes) wherever the
 * constants are needed, to reduce verbosity.
 */
public final class Constants {

  /** Constants for ports that are on the roboRIO. */
  public static final class RoboRIO {
    /** Constants for the Control Area Network bus. */
    public static final class CAN {
      /** The port of the back left drive motor. */
      public static final int kPortMotorDriveBackLeft = 1;
      /** The port of the front right drive motor. */
      public static final int kPortMotorDriveFrontRight = 2;
      /** The port of the back right drive motor. */
      public static final int kPortMotorDriveBackRight = 3;
      /** The port of the left hanger motor. */
      public static final int kPortMotorHangerLeft = 5;
      /** The port of the right hanger motor. */
      public static final int kPortMotorHangerRight = 6;
      /** The port of the front left drive motor. */
      public static final int kPortMotorDriveFrontLeft = 7;
      /** The ports of the intake double solenoid. */
      public static final int[] kPortDoubleSolenoidIntake = {0, 1};
      /** The port of the intake motor. */
      public static final int kPortMotorIntake = 4;
      /** The ports of the the launcher double solenoid. */
      public static final int[] kPortDoubleSolenoidLauncher = {2, 3};
    }

    /** Constants for the Digital Input/Output ports. */
    public static final class DIO {
      /** The port of the left hanger servo. */
      public static final int kPortServoLeftHanger = 1;
      /** The port of the right hanger servo. */
      public static final int kPortServoRightHanger = 2;
      /** The port of the grabber's right intake limit switch. */
      public static final int kPortLimitSwitchRightIntake = 4;
      /** The port of the grabber's left intake limit switch. */
      public static final int kPortLimitSwitchLeftIntake = 5;
      /** The port of the lower intake limit switch. */
      public static final int kPortLimitSwitchLowerIntake = 6;
      /** The port of the upper intake limit switch. */
      public static final int kPortLimitSwitchUpperIntake = 7;
      /** The port of the right hanger limit switch. */
      public static final int kPortLimitSwitchRightHanger = 8;
      /** The port of the left hanger limit switch. */
      public static final int kPortLimitSwitchLeftHanger = 9;
      /** The ports of the left drive encoders. */
      public static final int[] kPortEncoderDriveLeft = {0, 1};
      /** The ports of the right drive encoders. */
      public static final int[] kPortEncoderDriveRight = {2, 3};
    }
  }

  /** Constants for ports that are on the driver station. */
  public static final class DriverStation {
    /** The port of the driver controller. */
    public static final int kPortControllerDrive = 0;
    /** The port of the manipulator controller. */
    public static final int kPortControllerManip = 1;
  }

  /**
   * Constants for the field arena.
   *
   * <p>Since these measurements are prone to unit errors, they are listed before each variable is
   * defined.
   */
  public static final class FieldConstants {
    /**
     * The diameter of the cargo balls.
     *
     * <p>Meters.
     */
    public static final double kCargoDiameter = Units.inchesToMeters(9.5);
    /**
     * The distance to initially back up during the autonomous period.
     *
     * <p>Meters.
     */
    public static final double kTarmacExitDistance = Units.feetToMeters(4.5);
    /**
     * The distance from ground level to the upper hub.
     *
     * <p>Meters.
     */
    public static final double kHubUpperHeight = Units.inchesToMeters(104);
    /**
     * The distance from the exterior of the hub to its center.
     *
     * <p>Meters.
     */
    public static final double kHubCenterDistance = Units.inchesToMeters(53.5);
  }

  /**
   * Constants for the drivetrain subsystem.
   *
   * <p>Since these measurements are prone to unit errors, they are listed before each variable is
   * defined.
   */
  public static final class DrivetrainConstants {
    // Speed Constants
    /**
     * Lowest bound to set drive motors before scaling.
     *
     * <p>Unitless.
     */
    public static final double kSpeedSlow = 0.8;
    /**
     * Normal bound to set drive motors before scaling.
     *
     * <p>Unitless.
     */
    public static final double kSpeedNormal = 1.1;
    /**
     * Highest bound to set drive motors before scaling.
     *
     * <p>Unitless.
     */
    public static final double kSpeedFast = 1.4;
    /**
     * The time required for the robot to back out of the initial tarmac area with the normal speed
     * modifier during the autonomous mode.
     *
     * <p>Used for dead reckoning purposes.
     *
     * <p>Seconds.
     */
    public static final double kTimeLeaveTarmac = 2.5;
    // TODO: Experimentally determine these values.
    /**
     * The max linear velocity when following a path.
     *
     * <p>Meters / Second.
     */
    public static final double kMaxTrajectoryVelocity = 1.5;
    /**
     * The max linear acceleration when following a path
     *
     * <p>Meters / Second^2.
     */
    public static final double kMaxTrajectoryAcceleration = 0.5;
    /**
     * The max velocity to turn at when turning on the spot.
     *
     * <p>Radians / Second.
     */
    public static final double kMaxTurnVelocity = Units.degreesToRadians(100);
    /**
     * The max acceleration to turn with when turning on the spot.
     *
     * <p>Degrees / Second^2.
     */
    public static final double kMaxTurnAcceleration = Units.degreesToRadians(90);
    /**
     * The error produced by the drive motors when set to any given degree of rotation
     *
     * <p>Unitless.
     */
    public static final double kTurnError = 1.0921;
    /**
     * The maximum voltage available to the drive motors while following a path.
     *
     * <p>Volts.
     */
    public static final double kMaxVolts = 12.0;

    // Physical Robot Constants
    /**
     * The diameter of the wheels.
     *
     * <p>Meters.
     */
    public static final double kWheelDiameter = Units.inchesToMeters(6);
    /**
     * TODO: Determine this value.
     *
     * <p>The trackwidth of the drivetrain.
     *
     * <p>Meters.
     */
    public static final double kTrackWidth = Units.inchesToMeters(21.8);

    /**
     * Constants for the drivetrain encoders.
     *
     * <p>TODO: Calculate all values.
     */
    public static final class EncoderConstants {
      /**
       * The distance traveled in one revolution, equivalent to the circumference of the wheels.
       *
       * <p>Meters / Revolution.
       */
      private static final double kMetersPerRev = Math.PI * kWheelDiameter;
      /**
       * The pulses per revolution outputted by the encoder.
       *
       * <p>Pulses / Revolutions.
       */
      private static final int kEncoderPulsesPerRevolution = 2048;
      /**
       * The distance traveled in one pulse.
       *
       * <p>Meters / Pulse, which will be multiplied by the pulse reading, becoming just meters.
       */
      public static final double kEncoderDistancePerPulse =
          kMetersPerRev / kEncoderPulsesPerRevolution;
      /**
       * The number of encoder samples to average. Set to greater than the default to reduce noise.
       *
       * <p>Samples.
       */
      public static final int kEncoderSamplesToAverage = 5;

      /** Constants for the feedforward of the PID controller. */
      public static final class Feedforward {
        /**
         * The voltage required to overcome the motor's static friction.
         *
         * <p>Volts.
         */
        public static final double kStaticFrictionVoltage = 0.933;
        /**
         * The voltage required to cruise at a certain velocity, accounting for the increase in
         * friction as the robot accelerates.
         *
         * <p>Volts * (Seconds / Meter).
         */
        public static final double kVelocityVoltage = 2.47;
        /**
         * The voltage required to induce a certain acceleration.
         *
         * <p>Unit: Volts * (Seconds^2 / Meter).
         */
        public static final double kAccelerationVoltage = 0.548;
        /**
         * The voltage required to cruise at a certain angular velocity, accounting for the increase
         * in friction as the robot accelerates.
         *
         * <p>Volts * (Seconds / Radians).
         */
        public static final double kAngularVelocityVoltage = 2.39;
        /**
         * The voltage required to induce a certain angular acceleration.
         *
         * <p>Volts * (Seconds^2 / Radians).
         */
        public static final double kAngularAccelerationVoltage = 0.672;
      }

      /** Constants for the feedback of the PID controller. */
      public static final class Feedback {
        /** Feedback on the drivetrain x-axis. */
        public static final class DrivetrainX {
          /**
           * The proportional term for the position PID controller.
           *
           * <p>(Meters / Second) * (1 / Meter).
           */
          public static final double kPositionProportional = 0.5;
          /**
           * The derivative term for the position PID controller.
           *
           * <p>(Meters / Second) * (Seconds / Meter).
           */
          public static final double kPositionDerivative = 0;
          /**
           * The proportional term for the velocity PID controller.
           *
           * <p>(Meters / Second) * (Seconds / Meter).
           */
          public static final double kVelocityProportional = 2.55;
        }

        /** Feedback on the drivetrain theta. */
        public static final class DrivetrainTheta {
          /**
           * The proportional term for the drive turn position PID controller.
           *
           * <p>(Radians / Second) * (1 / Radian).
           */
          public static final double kPositionProportional = 0.5;
          /**
           * The derivative term for the drive turn position PID controller.
           *
           * <p>(Radians / Second) * (Seconds / Radian).
           */
          public static final double kPositionDerivative = 0;
          /**
           * The proportional term for the drive turn position PID controller.
           *
           * <p>(Radians / Second) * (Seconds / Radian).
           */
          public static final double kVelocityProportional = 2.55;
        }
        /**
         * The beta term for the Ramsete controller.
         *
         * <p>Meters.
         */
        public static final double kRamseteB = 2;
        /**
         * The zeta term for the Ramsete controller.
         *
         * <p>Seconds.
         */
        public static final double kRamseteZeta = 0.7;
      }
    }
  }

  /** Constants for the hanger subsystem. */
  public static final class HangerConstants {
    /** Counts per revolution of the hanger encoders. */
    public static final int kEncoderCyclesPerRev = 8192;
    /** Bound to set the hanger motors when extending. */
    public static final double kSpeedExtend = 0.25;
    /** Bound to set the hanger motors when retracting. */
    public static final double kSpeedRetract = 0.5;
    /** Encoder reading when extending the hanger. */
    public static final double kEncodersRaised = 11.0;
    /** Servo position for the left hanger mechanical stop. */
    public static final double kServoLeftLocked = 1.0;
    /** Servo position for the right hanger mechanical stop. */
    public static final double kServoRightLocked = 0.0;
    /** Servo position for when the left hanger mechanical is not stopped. */
    public static final double kServoLeftUnlocked = 0.3;
    /** Servo position for when the right hanger mechanical is not stopped. */
    public static final double kServoRightUnlocked = 0.4;
    /** Time required for the hanger toggle to lock or unlock. */
    public static final double kTimeServoToggle = 0.5;
    /** Diamater of the hanger hex shaft. */
    public static final double kHexShaftDiam = 0.5;
  }

  /** Constants for the intake subystem. */
  public static final class IntakeConstants {
    /** Bound to set the intake motor when raising. */
    public static final double kSpeedRaise = 0.75;
    /**
     * Bound to set the intake motor when lowering.
     *
     * <p>The speed is represented as negative so that the motor is turned clockwise.
     */
    public static final double kSpeedLower = -0.5;
  }

  /**
   * Constants for the launcher subsystem.
   *
   * <p>Since these measurements are prone to unit errors, they are listed before each variable is
   * defined.
   */
  public static final class LauncherConstants {
    // TODO: Calculate these distances.
    /**
     * Maximum horizontal distance the launcher from can fire from to match the peak of the vertical
     * parabolic trajectory.
     *
     * <p>Meters.
     */
    public static final double kMaxHorizontalPeakDistance = Units.inchesToMeters(85.0);
    /**
     * Maximum vertical distance the launcher can fire reliably.
     *
     * <p>Meters.
     */
    public static final double kMaxVerticalUseableDistance = Units.inchesToMeters(53.0);
    /**
     * The time required for the launcher solenoid to extend or retract.
     *
     * <p>Seconds.
     */
    public static final double kTimeToggle = 1.0;
  }
}
