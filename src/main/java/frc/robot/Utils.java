// Copyright (c) OpenRobotGroup.
// Open Source Software; you can modify and/or share it under the terms of
// the BSD license file in the root directory of this project.

package frc.robot;

/** Utility functions that do not fit in otherwise namesake classes. */
public class Utils {

  /**
   * Applies a nonlinear slew function to a double.
   *
   * @param value The value to which the function is applied.
   * @return The value after the function has been applied.
   */
  public static double slew(double value) {
    return 0.5 * value * (1 + 0.5 * Math.pow(value, 2));
  }
}
