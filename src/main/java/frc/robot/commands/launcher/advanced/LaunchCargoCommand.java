// Copyright (c) OpenRobotGroup.
// Open Source Software; you can modify and/or share it under the terms of
// the BSD license file in the root directory of this project.

package frc.robot.commands.launcher.advanced;

import edu.wpi.first.wpilibj2.command.SequentialCommandGroup;

public class LaunchCargoCommand extends SequentialCommandGroup {}
