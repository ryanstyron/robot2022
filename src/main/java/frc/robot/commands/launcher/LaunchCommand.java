// Copyright (c) OpenRobotGroup.
// Open Source Software; you can modify and/or share it under the terms of
// the BSD license file in the root directory of this project.

package frc.robot.commands.launcher;

import edu.wpi.first.wpilibj.DoubleSolenoid;
import edu.wpi.first.wpilibj.Timer;
import edu.wpi.first.wpilibj2.command.CommandBase;

import frc.robot.Constants.LauncherConstants;
import frc.robot.subsystems.LauncherSubsystem;

/**
 * Sets the launcher solenoid in different states depending on time elapsed after the command has
 * been called.
 */
public class LaunchCommand extends CommandBase {

  // Subsystems
  private LauncherSubsystem m_launcherSubsystem;

  // Timer
  private Timer m_timer;

  /** Instantiates the launcher subsystem when creating the command. */
  public LaunchCommand(LauncherSubsystem launcherSubsystem) {
    m_launcherSubsystem = launcherSubsystem;

    addRequirements(m_launcherSubsystem);
  }

  /** Initializes the command. */
  @Override
  public void initialize() {
    // Initialize the timer.
    m_timer = new Timer();
    m_timer.reset();
    m_timer.start();
  }

  /** Runs periodically while the command is scheduled. */
  @Override
  public void execute() {
    // Set the solenoid in the forward state until the given time has elapsed.
    if (m_timer.get() >= 0 && m_timer.get() < LauncherConstants.kTimeToggle)
      m_launcherSubsystem.setLauncher(DoubleSolenoid.Value.kForward);
    // Set the solenoid in the reverse state for the same period of time.
    else if (m_timer.get() >= LauncherConstants.kTimeToggle
        && m_timer.get() < LauncherConstants.kTimeToggle * 2)
      m_launcherSubsystem.setLauncher(DoubleSolenoid.Value.kReverse);
    // Disable the solenoid after the allotted time to prevent the piston from spamming.
    else m_launcherSubsystem.setLauncher(DoubleSolenoid.Value.kOff);
  }

  /** Runs when the command ends. */
  @Override
  public void end(boolean isInterrupted) {
    // Ensure the launcher solenoid is disabled when not in use.
    m_launcherSubsystem.setLauncher(DoubleSolenoid.Value.kOff);
  }

  /**
   * @return Whether the command has finished. Once a command finishes, the scheduler will call its
   *     end() method and un-schedule it.
   */
  @Override
  public boolean isFinished() {
    // End the command if the total time to execute has elapsed.
    return m_timer.hasElapsed(LauncherConstants.kTimeToggle * 2);
  }
}
