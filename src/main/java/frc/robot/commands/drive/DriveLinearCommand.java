// Copyright (c) OpenRobotGroup.
// Open Source Software; you can modify and/or share it under the terms of
// the BSD license file in the root directory of this project.

package frc.robot.commands.drive;

import edu.wpi.first.wpilibj2.command.CommandBase;

import frc.robot.Constants.DrivetrainConstants;
import frc.robot.subsystems.DrivetrainSubsystem;

/**
 * Drives the robot linearly a certain distance at a certain motor voltage.
 *
 * <p>Dependent on odometry, however does not implement advanced PID control.
 */
public class DriveLinearCommand extends CommandBase {

  // Subsystems
  private final DrivetrainSubsystem m_drivetrainSubsystem;

  // Proportion to set the drive motors.
  private double m_speed;

  // Distance to drive the robot.
  private double m_distance;

  // Whether the encoders indicate that the rotation has completed.
  private boolean m_hasTraveled = false;

  /**
   * Constructs a command that sets the linear speed of the drivetrain until the distance is
   * traveled.
   *
   * @param drivetrainSubsystem The drivetrain subsystem.
   * @param resetOdometry If the encoder values should be reset before executing the command.
   * @param speed Proportion to set the drive motors on the linear axis.
   * @param distance Distance, in meters, to drive the robot.
   */
  public DriveLinearCommand(
      DrivetrainSubsystem drivetrainSubsystem, double speed, double distance) {
    m_drivetrainSubsystem = drivetrainSubsystem;
    m_speed = speed;
    m_distance = distance;

    addRequirements(m_drivetrainSubsystem);
  }

  /** Initializes the command. */
  @Override
  public void initialize() {
    // Reset the odometry.
    m_drivetrainSubsystem.resetOdometry(null);

    // Set the boolean to true so the command can does not immediately terminate.
    m_hasTraveled = false;
  }

  /** Runs periodically while the command is scheduled. */
  @Override
  public void execute() {
    // Set the drive motors to the given proportion on the linear axis when the distance has not
    // been traveled.
    if (Math.abs(m_drivetrainSubsystem.getDistance()) < m_distance)
      m_drivetrainSubsystem.tankDrive(
          DrivetrainConstants.kMaxVolts * m_speed, DrivetrainConstants.kMaxVolts * m_speed);
    else {
      m_drivetrainSubsystem.tankDrive(0.0, 0.0);
      m_hasTraveled = true;
    }
  }

  /** Runs when the command ends. */
  @Override
  public void end(boolean interrupted) {
    // Set the drive motors so that the robot is stationary.
    m_drivetrainSubsystem.tankDrive(0.0, 0.0);

    // Set the distance driven as determined by the encoders.
    m_drivetrainSubsystem.setLastLinearDistance(m_drivetrainSubsystem.getDistance());
  }

  /**
   * @return Whether the command has finished. Once a command finishes, the scheduler will call its
   *     end() method and un-schedule it.
   */
  @Override
  public boolean isFinished() {
    // End the command if the total distance has been traveled.
    return m_hasTraveled;
  }
}
