// Copyright (c) OpenRobotGroup.
// Open Source Software; you can modify and/or share it under the terms of
// the BSD license file in the root directory of this project.

package frc.robot.commands.drive.advanced;

import java.util.List;

import edu.wpi.first.math.controller.PIDController;
import edu.wpi.first.math.controller.RamseteController;
import edu.wpi.first.math.controller.SimpleMotorFeedforward;
import edu.wpi.first.math.geometry.Pose2d;
import edu.wpi.first.math.geometry.Translation2d;
import edu.wpi.first.math.kinematics.ChassisSpeeds;
import edu.wpi.first.math.kinematics.DifferentialDriveKinematics;
import edu.wpi.first.math.kinematics.DifferentialDriveWheelSpeeds;
import edu.wpi.first.math.trajectory.Trajectory;
import edu.wpi.first.math.trajectory.Trajectory.State;
import edu.wpi.first.math.trajectory.TrajectoryConfig;
import edu.wpi.first.math.trajectory.TrajectoryGenerator;
import edu.wpi.first.math.trajectory.constraint.DifferentialDriveVoltageConstraint;
import edu.wpi.first.wpilibj.Timer;
import edu.wpi.first.wpilibj2.command.CommandBase;

import frc.robot.Constants.DrivetrainConstants;
import frc.robot.Constants.DrivetrainConstants.EncoderConstants;
import frc.robot.subsystems.DrivetrainSubsystem;

/**
 * A command that uses a ({@link RamseteController}) to follow a {@link Trajectory} with a
 * differential drive. This is based off of the WPILib RamseteCommand implementation. The reason
 * this exists, rather then extending RamseteCommand, is that RamseteCommand can not easily be
 * debugged when parts of it are suppressed its own class.
 *
 * <p>The command handles trajectory-following, PID calculations, and feedforwards internally. This
 * is intended to be a more-or-less "complete solution" that can be used by teams without a great
 * deal of controls expertise.
 */
public class DriveTrajectoryCommand extends CommandBase {

  // Subsystems
  private final DrivetrainSubsystem m_drivetrainSubsystem;

  // Kinematics
  private final DifferentialDriveKinematics m_kinematics =
      new DifferentialDriveKinematics(DrivetrainConstants.kTrackWidth);

  // Feedforward Controller
  private final SimpleMotorFeedforward m_feedforward =
      new SimpleMotorFeedforward(
          EncoderConstants.Feedforward.kStaticFrictionVoltage,
          EncoderConstants.Feedforward.kVelocityVoltage,
          EncoderConstants.Feedforward.kAccelerationVoltage);

  // Ramsete Feedback Controller (Follows path)
  private final RamseteController m_followerController =
      new RamseteController(
          EncoderConstants.Feedback.kRamseteB, EncoderConstants.Feedback.kRamseteZeta);

  // PID Feedback Controllers (Approaches setpoints)
  private final PIDController m_controllerLeft =
      new PIDController(EncoderConstants.Feedback.DrivetrainX.kVelocityProportional, 0, 0);
  private final PIDController m_controllerRight =
      new PIDController(EncoderConstants.Feedback.DrivetrainX.kVelocityProportional, 0, 0);

  // Voltage Constraint
  private final DifferentialDriveVoltageConstraint m_voltageConstraint =
      new DifferentialDriveVoltageConstraint(
          m_feedforward, m_kinematics, DrivetrainConstants.kMaxVolts);

  // Trajectory Configuration
  private final TrajectoryConfig m_trajectoryConfig =
      new TrajectoryConfig(
              DrivetrainConstants.kMaxTrajectoryVelocity,
              DrivetrainConstants.kMaxTrajectoryAcceleration)
          .setKinematics(m_kinematics)
          .addConstraint(m_voltageConstraint);

  // Trajectory
  private Trajectory m_trajectory;

  // Interior trajectory waypoints, if needed until the command is run.
  private List<Translation2d> m_interiorWaypoints;

  // Ending pose, if needed until the command is run.
  private Pose2d m_end;

  // Timer
  private final Timer m_timer = new Timer();

  // Wheel speeds as of last execute().
  private DifferentialDriveWheelSpeeds m_previousSpeeds;

  // Time as of last execute().
  private double m_previousTime;

  /**
   * Constructs a command that, when executed, will follow the provided trajectory. PID control and
   * feedforward are handled internally, and outputs are scaled -12 to 12 representing units of
   * volts. The robot's current location is used as the starting pose.
   *
   * <p>Note: The controller will *not* set the outputVolts to zero upon completion of the path -
   * this is left to the user, since it is not appropriate for paths with nonstationary endstates.
   *
   * @param drivetrainSubsystem The drivetrain subsystem.
   * @param interiorWaypoints The interior waypoints.
   * @param end The ending pose.
   */
  public DriveTrajectoryCommand(
      DrivetrainSubsystem drivetrainSubsystem, List<Translation2d> interiorWaypoints, Pose2d end) {
    m_drivetrainSubsystem = drivetrainSubsystem;
    m_interiorWaypoints = interiorWaypoints;
    m_end = end;

    addRequirements(drivetrainSubsystem);
  }

  /**
   * Constructs a command that, when executed, will follow the provided trajectory. PID control and
   * feedforward are handled internally, and outputs are scaled -12 to 12 representing units of
   * volts.
   *
   * <p>Note: The controller will *not* set the outputVolts to zero upon completion of the path -
   * this is left to the user, since it is not appropriate for paths with nonstationary endstates.
   *
   * @param drivetrainSubsystem The drivetrain subsystem.
   * @param start The starting pose.
   * @param interiorWaypoints The interior waypoints.
   * @param end The ending pose.
   */
  public DriveTrajectoryCommand(
      DrivetrainSubsystem drivetrainSubsystem,
      Pose2d start,
      List<Translation2d> interiorWaypoints,
      Pose2d end) {
    m_drivetrainSubsystem = drivetrainSubsystem;
    m_trajectory =
        TrajectoryGenerator.generateTrajectory(start, interiorWaypoints, end, m_trajectoryConfig);

    addRequirements(drivetrainSubsystem);
  }

  /** Initializes the command. */
  @Override
  public void initialize() {
    // If the robot's current position is used as the start point, generate the trajectory
    // now.
    if (m_trajectory == null)
      m_trajectory =
          TrajectoryGenerator.generateTrajectory(
              m_drivetrainSubsystem.getPose(), m_interiorWaypoints, m_end, m_trajectoryConfig);

    m_previousTime = -1;
    State initialState = m_trajectory.sample(0);
    m_previousSpeeds =
        m_kinematics.toWheelSpeeds(
            new ChassisSpeeds(
                initialState.velocityMetersPerSecond,
                0,
                initialState.curvatureRadPerMeter * initialState.velocityMetersPerSecond));
    m_timer.reset();
    m_timer.start();
    m_controllerLeft.reset();
    m_controllerRight.reset();
  }

  /** Runs periodically while the command is scheduled. */
  @Override
  public void execute() {
    double currentTime = m_timer.get();
    double changeInTime = currentTime - m_previousTime;

    // If the command has just been initialized.
    if (m_previousTime < 0) {
      // Reset the drive motors to 0.
      m_drivetrainSubsystem.tankDrive(0.0, 0.0);
      m_previousTime = currentTime;
      return;
    }

    // Calculate the target chassis speeds for this time in the trajectory, and convert them to
    // wheel speeds.
    DifferentialDriveWheelSpeeds targetWheelSpeeds =
        m_kinematics.toWheelSpeeds(
            m_followerController.calculate(
                m_drivetrainSubsystem.getPose(), m_trajectory.sample(currentTime)));
    double leftSpeedSetpoint = targetWheelSpeeds.leftMetersPerSecond;
    double rightSpeedSetpoint = targetWheelSpeeds.rightMetersPerSecond;

    // Calculate the feedforward for the given velocity setpoint. For the acceleration, calculate
    // the secant from the previous speed to this speed (delta y/delta x=delta v/delta t).
    double leftFeedforward =
        m_feedforward.calculate(
            leftSpeedSetpoint,
            (leftSpeedSetpoint - m_previousSpeeds.leftMetersPerSecond) / changeInTime);
    double rightFeedforward =
        m_feedforward.calculate(
            rightSpeedSetpoint,
            (rightSpeedSetpoint - m_previousSpeeds.rightMetersPerSecond) / changeInTime);

    // Get the actual current wheel speeds.
    DifferentialDriveWheelSpeeds wheelSpeeds = m_drivetrainSubsystem.getWheelSpeeds();

    // In addition to the feed forward, add the feedback to further approach the setpoint. The
    // feedforward should allow the robot to nearly reach the goal, then take the current wheel
    // speed measurements, and use the PID controller to adjust for the difference between that and
    // the desired setpoint from the Ramsete controller.
    double leftOutput =
        leftFeedforward
            + m_controllerLeft.calculate(wheelSpeeds.leftMetersPerSecond, leftSpeedSetpoint);
    double rightOutput =
        rightFeedforward
            + m_controllerRight.calculate(wheelSpeeds.rightMetersPerSecond, rightSpeedSetpoint);

    // Drive with the calculated motor outputs.
    m_drivetrainSubsystem.tankDrive(leftOutput, rightOutput);

    // Update the speeds and times.
    m_previousSpeeds = targetWheelSpeeds;
    m_previousTime = currentTime;
  }

  /** Runs when the command ends. */
  @Override
  public void end(boolean interrupted) {
    m_timer.stop();

    // If the command was interrupted, set the drive motors.
    if (interrupted) m_drivetrainSubsystem.tankDrive(0, 0);
  }

  /**
   * @return Whether the command has finished. Once a command finishes, the scheduler will call its
   *     end() method and un-schedule it.
   */
  @Override
  public boolean isFinished() {
    return m_timer.hasElapsed(m_trajectory.getTotalTimeSeconds());
  }
}
