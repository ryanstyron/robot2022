// Copyright (c) OpenRobotGroup.
// Open Source Software; you can modify and/or share it under the terms of
// the BSD license file in the root directory of this project.

package frc.robot.commands.drive.advanced;

import edu.wpi.first.math.controller.PIDController;
import edu.wpi.first.math.controller.ProfiledPIDController;
import edu.wpi.first.math.controller.SimpleMotorFeedforward;
import edu.wpi.first.math.filter.SlewRateLimiter;
import edu.wpi.first.math.kinematics.ChassisSpeeds;
import edu.wpi.first.math.kinematics.DifferentialDriveKinematics;
import edu.wpi.first.math.kinematics.DifferentialDriveWheelSpeeds;
import edu.wpi.first.math.trajectory.TrapezoidProfile;
import edu.wpi.first.math.util.Units;
import edu.wpi.first.wpilibj.Timer;
import edu.wpi.first.wpilibj.smartdashboard.SmartDashboard;
import edu.wpi.first.wpilibj2.command.CommandBase;

import frc.robot.Constants.DrivetrainConstants;
import frc.robot.Constants.DrivetrainConstants.EncoderConstants;
import frc.robot.Constants.DrivetrainConstants.EncoderConstants.Feedback;
import frc.robot.subsystems.DrivetrainSubsystem;

/**
 * Drives the robot by setting motor voltages dependent on gyroscope and encoder data, with PID
 * feedback and feedforward.
 */
public class DriveCommand extends CommandBase {

  // Subsystems
  private final DrivetrainSubsystem m_drivetrainSubsystem;

  public DriveCommand(DrivetrainSubsystem drivetrainSubsystem) {
    m_drivetrainSubsystem = drivetrainSubsystem;

    addRequirements(drivetrainSubsystem);
  }

  /**
   * Represents a mode to be used for an axis. The mode will affect how the setpoint is used.
   *
   * <p>In order to use custom parameters, this class must be extended, and override execute() such
   * that the derived class updates the setpoint and the custom measurement, then calls this class'
   * execute().
   */
  public enum Mode {
    /**
     * Set a constant motor speed. A rate limiter is used so that the motor speed setpoints will
     * ramp up to this speed, rather than causing a drastic acceleration spike in the beginning.
     */
    Constant,
    /** Control the motor speed with PID, closing on position. */
    Control,
    /** Control the motor speed with PID, using custom parameters. */
    ControlCustom,
    /** Control the motor speed with profiled PID, closing on velocity. */
    ProfiledControl,
    /** Control the motor speed with profiled PID, using custom parameters. */
    ProfiledControlCustom,
  }

  // Values for the x-axis profile.
  /** The mode for the x-axis. */
  protected Mode m_xMode;
  /**
   * Setpoint for the x-axis.
   *
   * <p>If Mode.Constant is used, this is a velocity setpoint in meters per second. When any other
   * mode is used, this is a position setpoint in meters.
   */
  protected double m_xSetpoint;
  /**
   * Custom measurement for controlling x. If custom parameters are being used, this variable must
   * be populated with the custom measurement to use for the PID controller.
   */
  protected double m_xCustomMeasurement;
  /** Whether to negate the output of the PID controller being used with custom x setpoints. */
  protected boolean m_xNegateCustomOutput = false;
  /** The initial drivetrain distance. */
  protected double m_xInit;
  /** The rate limiter used for ramping up to a constant linear velocity. */
  private SlewRateLimiter m_xLimiter =
      new SlewRateLimiter(DrivetrainConstants.kMaxTrajectoryAcceleration);

  // Values for the theta profile.
  /** The mode for theta. */
  protected Mode m_tMode;
  /**
   * Setpoint for theta.
   *
   * <p>If Mode.Constant is used, this is a velocity setpoint in radians per second. When any other
   * mode is used, this is a position setpoint in radians.
   *
   * <p>This may be made relative to the drivetrain's rotation when the command is ran, depending on
   * if m_tRelative is set.
   */
  protected double m_tSetpoint;
  /**
   * Custom measurement for controlling theta.
   *
   * @see #m_xCustomMeasurement
   */
  protected double m_tCustomMeasurement;
  /**
   * Whether to negate the output of the PID controller being used with custom theta
   * measurements/setpoints.*
   */
  protected boolean m_tNegateCustomOutput = false;
  /**
   * Whether to treat the theta setpoint as relative to the initial robot heading, if a position
   * setpoint.
   */
  protected boolean m_tIsRelative;
  /** The initial drivetrain heading, if using a relative theta setpoint. */
  protected double m_tInit;
  /** The rate limiter used for ramping up to a constant angular velocity. */
  private SlewRateLimiter m_tLimiter =
      new SlewRateLimiter(DrivetrainConstants.kMaxTurnAcceleration);

  // PID controller for the drivetrain's position on the x-axis.
  protected PIDController m_pidControllerX =
      new PIDController(
          Feedback.DrivetrainX.kPositionProportional, 0, Feedback.DrivetrainX.kPositionDerivative);
  // PID controller for the drivetrain's position on the x-axis, using profiling.
  protected ProfiledPIDController m_pidControllerProfiledX =
      new ProfiledPIDController(
          Feedback.DrivetrainX.kPositionProportional * 100,
          0,
          Feedback.DrivetrainX.kPositionDerivative * 100,
          new TrapezoidProfile.Constraints(
              DrivetrainConstants.kMaxTrajectoryVelocity,
              DrivetrainConstants.kMaxTrajectoryAcceleration));
  // PID controller for the drivetrain's position on theta.
  protected PIDController m_pidControllerTheta =
      new PIDController(
          Feedback.DrivetrainTheta.kPositionProportional,
          0,
          Feedback.DrivetrainX.kPositionDerivative);
  // PID controller for the drivetrain's position on theta, using profiling.
  protected ProfiledPIDController m_pidControllerProfiledTheta =
      new ProfiledPIDController(
          Feedback.DrivetrainTheta.kPositionProportional * 100,
          0,
          Feedback.DrivetrainX.kPositionDerivative * 100,
          new TrapezoidProfile.Constraints(
              DrivetrainConstants.kMaxTurnVelocity, DrivetrainConstants.kMaxTurnAcceleration));

  // Kinematics
  private final DifferentialDriveKinematics m_kinematics =
      new DifferentialDriveKinematics(DrivetrainConstants.kTrackWidth);

  // Feedforward Controller (Calculates voltage from velocity)
  private final SimpleMotorFeedforward m_feedforward =
      new SimpleMotorFeedforward(
          EncoderConstants.Feedforward.kStaticFrictionVoltage,
          EncoderConstants.Feedforward.kVelocityVoltage,
          EncoderConstants.Feedforward.kAccelerationVoltage);

  // PID controller for the left wheel velocity.
  private PIDController m_controllerLeft =
      new PIDController(EncoderConstants.Feedback.DrivetrainX.kVelocityProportional, 0, 0);
  // PID controller for the right wheel velocity.
  private PIDController m_controllerRight =
      new PIDController(EncoderConstants.Feedback.DrivetrainX.kVelocityProportional, 0, 0);

  // Timer
  private final Timer m_timer = new Timer();

  // Wheel speeds as of last execute().
  private double m_previousLeftSpeedSetpoint;
  private double m_previousRightSpeedSetpoint;

  // Time as of last execute().
  private double m_previousTime;

  // Whether the command has just started. This is necessary so that the first time execute() is
  // called nothing will be done, meaning the position PID controllers will be at their
  // setpoints, thus isFinished() will undesirably return true.
  private boolean m_hasJustStarted = false;

  /**
   * Initializes the command.
   *
   * @param drivetrainSubsystem The drivetrain subsystem.
   * @param xMode The action to take for the x-axis. @see #m_xMode.
   * @param xSetpoint The setpoint for the x-axis. @see #m_xSetpoint.
   * @param tMode The action to take for the theta. @see #m_zAction.
   * @param tSetpoint The setpoint for the theta. @see #m_zSetpoint.
   * @param tRelative Whether to treat the theta setpoint as relative @see #m_tRelative.
   */
  public DriveCommand(
      DrivetrainSubsystem drivetrainSubsystem,
      Mode xMode,
      double xSetpoint,
      Mode tMode,
      double tSetpoint,
      boolean tRelative) {
    m_drivetrainSubsystem = drivetrainSubsystem;
    m_xSetpoint = xSetpoint;
    m_xMode = xMode;
    m_tSetpoint = tSetpoint;
    m_tMode = tMode;
    m_tIsRelative = tRelative;

    // Configure all of the controllers as to not have errors thrown when a derived class may call
    // upon otherwise unused modes.
    m_pidControllerX.setTolerance(Units.inchesToMeters(1), Units.inchesToMeters(1));
    m_pidControllerTheta.enableContinuousInput(-Math.PI, Math.PI);
    m_pidControllerTheta.setTolerance(Units.degreesToRadians(1));
    // Set the controller to be continuous since it is an angle controller.
    m_pidControllerProfiledTheta.enableContinuousInput(-Math.PI, Math.PI);
    // Set the controller tolerance; the delta tolerance ensures the robot is stationary at the
    // setpoint before it is considered as having reached the reference.
    m_pidControllerProfiledTheta.setTolerance(Units.degreesToRadians(1));

    addRequirements(drivetrainSubsystem);
  }

  /**
   * Constructs the command with a non-relative angle, but not necessarily an absolute angle.
   *
   * <p>Used as a convenience constructor when there is a constant action on the theta profile.
   *
   * @param drivetrainSubsystem The drivetrain subsystem.
   * @param xMode The action to take for the x-axis. @see #m_xMode.
   * @param xSetpoint The setpoint for the x-axis. @see #m_xSetpoint.
   * @param tMode The action to take for the theta. @see #m_zAction.
   * @param tSetpoint The setpoint for the theta. @see #m_zSetpoint.
   */
  public DriveCommand(
      DrivetrainSubsystem drivetrainSubsystem,
      Mode xMode,
      double xSetpoint,
      Mode tMode,
      double tSetpoint) {
    this(drivetrainSubsystem, xMode, xSetpoint, tMode, tSetpoint, false);
  }

  /** Initializes the command. */
  @Override
  public void initialize() {
    // Initialize the appropriate PID controllers, if any.

    m_xInit = m_drivetrainSubsystem.getDistance();
    switch (m_xMode) {
      case Constant:
        m_xLimiter.reset(m_drivetrainSubsystem.getSpeed());
        break;
      case Control:
      case ControlCustom:
        m_pidControllerX.reset();
        break;
      case ProfiledControl:
      case ProfiledControlCustom:
        m_pidControllerProfiledX.reset(m_xInit, m_drivetrainSubsystem.getSpeed());
        break;
    }

    m_tInit = m_drivetrainSubsystem.getHeading();
    switch (m_tMode) {
      case Constant:
        m_tLimiter.reset(m_drivetrainSubsystem.getTurnRate());
        break;
      case Control:
      case ControlCustom:
        m_pidControllerTheta.reset();
        break;
      case ProfiledControl:
      case ProfiledControlCustom:
        m_pidControllerProfiledTheta.reset(m_tInit, m_drivetrainSubsystem.getTurnRate());
        break;
    }

    m_hasJustStarted = true;
    m_previousLeftSpeedSetpoint = m_previousRightSpeedSetpoint = 0;
    m_previousTime = -1;
    m_timer.reset();
    m_timer.start();
  }

  /** Runs periodically while the command is scheduled. */
  @Override
  public void execute() {
    double currentTime = m_timer.get();
    double changeInTime = currentTime - m_previousTime;

    // Reset drive motors if the command has just been initialized.
    if (m_previousTime < 0) {
      m_drivetrainSubsystem.tankDrive(0.0, 0.0);
      m_previousTime = currentTime;
      return;
    } else {
      m_hasJustStarted = false;
    }

    double xSpeedSetpoint = 0;

    switch (m_xMode) {
      case Constant:
        xSpeedSetpoint = m_xLimiter.calculate(m_xSetpoint);
        break;
      case Control:
        xSpeedSetpoint =
            m_pidControllerX.calculate(m_drivetrainSubsystem.getDistance(), m_xInit + m_xSetpoint);
        SmartDashboard.putNumber("x Error", m_pidControllerX.getPositionError());
        break;
      case ControlCustom:
        xSpeedSetpoint =
            (m_xNegateCustomOutput ? -1.0 : 1.0)
                * m_pidControllerX.calculate(m_xCustomMeasurement, m_xSetpoint);
        SmartDashboard.putNumber("x Error", m_pidControllerX.getPositionError());
        break;
      case ProfiledControl:
        xSpeedSetpoint =
            m_pidControllerProfiledX.calculate(
                m_drivetrainSubsystem.getDistance(), m_xInit + m_xSetpoint);
        SmartDashboard.putNumber("x Error", m_pidControllerProfiledX.getPositionError());
        break;
      case ProfiledControlCustom:
        xSpeedSetpoint =
            (m_xNegateCustomOutput ? -1.0 : 1.0)
                * m_pidControllerProfiledX.calculate(m_xCustomMeasurement, m_tSetpoint);
        SmartDashboard.putNumber("x Error", m_pidControllerProfiledX.getPositionError());
        break;
    }
    SmartDashboard.putNumber("x Speed Setpoint", xSpeedSetpoint);

    double tSpeedSetpoint = 0;

    double tSetpointAdjusted = m_tSetpoint + (m_tIsRelative ? m_tInit : 0);
    switch (m_tMode) {
      case Constant:
        tSpeedSetpoint = m_tLimiter.calculate(m_tSetpoint);
        break;
      case Control:
        tSpeedSetpoint =
            m_pidControllerTheta.calculate(m_drivetrainSubsystem.getHeading(), tSetpointAdjusted);
        SmartDashboard.putNumber("t Error", m_pidControllerTheta.getPositionError());
        break;
      case ControlCustom:
        tSpeedSetpoint =
            m_tNegateCustomOutput
                ? -1.0
                : 1.0 * m_pidControllerTheta.calculate(m_tCustomMeasurement, m_tSetpoint);
        SmartDashboard.putNumber("t Error", m_pidControllerTheta.getPositionError());
        break;
      case ProfiledControl:
        tSpeedSetpoint =
            m_pidControllerProfiledTheta.calculate(
                m_drivetrainSubsystem.getHeading(), tSetpointAdjusted);
        SmartDashboard.putNumber("t Error", m_pidControllerProfiledTheta.getPositionError());
        break;
      case ProfiledControlCustom:
        tSpeedSetpoint =
            m_tNegateCustomOutput
                ? -1.0
                : 1.0 * m_pidControllerProfiledTheta.calculate(m_tCustomMeasurement, m_tSetpoint);
        SmartDashboard.putNumber("t Error", m_pidControllerProfiledTheta.getPositionError());
        break;
    }
    SmartDashboard.putNumber("t Speed Setpoint", tSpeedSetpoint);

    DifferentialDriveWheelSpeeds targetWheelSpeeds =
        m_kinematics.toWheelSpeeds(new ChassisSpeeds(xSpeedSetpoint, 0, tSpeedSetpoint));
    double leftSpeedSetpoint =
        Math.copySign(
            Math.min(
                DrivetrainConstants.kMaxTrajectoryVelocity,
                Math.abs(targetWheelSpeeds.leftMetersPerSecond)),
            targetWheelSpeeds.leftMetersPerSecond);
    double rightSpeedSetpoint =
        Math.copySign(
            Math.min(
                DrivetrainConstants.kMaxTrajectoryVelocity,
                Math.abs(targetWheelSpeeds.rightMetersPerSecond)),
            targetWheelSpeeds.rightMetersPerSecond);

    SmartDashboard.putNumber("Left Speed Setpoint", targetWheelSpeeds.leftMetersPerSecond);
    SmartDashboard.putNumber("Right Speed Setpoint", targetWheelSpeeds.rightMetersPerSecond);

    // Calculate the feedforward for the given velocity setpoint. For the acceleration, calculate
    // the secant from the previous speed to this speed (delta y/delta x = delta v/delta t).
    double leftFeedforward =
        m_feedforward.calculate(m_previousLeftSpeedSetpoint, leftSpeedSetpoint, changeInTime);
    double rightFeedforward =
        m_feedforward.calculate(m_previousRightSpeedSetpoint, rightSpeedSetpoint, changeInTime);

    SmartDashboard.putNumber("Left Feedforward", leftFeedforward);
    SmartDashboard.putNumber("Right Feedforward", rightFeedforward);

    // Get the actual current wheel speeds.
    DifferentialDriveWheelSpeeds wheelSpeeds = m_drivetrainSubsystem.getWheelSpeeds();

    // In addition to the feed forward, add the feedback to further approach the setpoint. The
    // feedforward should get us most of the way there, then we take the current wheel speed
    // measurements, and use the PID controller to adjust for the difference between that and the
    // desired setpoint we got from the Ramsete controller.
    double leftOutput =
        leftFeedforward
            + m_controllerLeft.calculate(wheelSpeeds.leftMetersPerSecond, leftSpeedSetpoint);
    double rightOutput =
        rightFeedforward
            + m_controllerRight.calculate(wheelSpeeds.rightMetersPerSecond, rightSpeedSetpoint);

    SmartDashboard.putNumber("Left Speed", wheelSpeeds.leftMetersPerSecond);
    SmartDashboard.putNumber("Right Speed", wheelSpeeds.rightMetersPerSecond);

    SmartDashboard.putNumber("Left Output", leftOutput);
    SmartDashboard.putNumber("Right Output", rightOutput);

    // Drive with the calculated motor outputs.
    m_drivetrainSubsystem.tankDrive(leftOutput, rightOutput);

    // Update the speeds and times.
    m_previousLeftSpeedSetpoint = leftSpeedSetpoint;
    m_previousRightSpeedSetpoint = rightSpeedSetpoint;
    m_previousTime = currentTime;
  }

  /** Runs when the command ends. */
  @Override
  public void end(boolean interrupted) {
    if (interrupted) m_drivetrainSubsystem.tankDrive(0, 0);

    SmartDashboard.putNumber("x Error", 0);
    SmartDashboard.putNumber("t Error", 0);
    SmartDashboard.putNumber("x Speed Setpoint", 0);
    SmartDashboard.putNumber("t Speed Setpoint", 0);
    SmartDashboard.putNumber("Left Speed Setpoint", 0);
    SmartDashboard.putNumber("Right Speed Setpoint", 0);
    SmartDashboard.putNumber("Left Feedforward", 0);
    SmartDashboard.putNumber("Right Feedforward", 0);
    SmartDashboard.putNumber("Left Speed", 0);
    SmartDashboard.putNumber("Right Speed", 0);
    SmartDashboard.putNumber("Left Output", 0);
    SmartDashboard.putNumber("Right Output", 0);
  }

  /**
   * @return Whether the command has finished. Once a command finishes, the scheduler will call its
   *     end() method and un-schedule it.
   */
  @Override
  public boolean isFinished() {
    if (m_xMode == Mode.Constant && m_tMode == Mode.Constant) return false;
    if (m_hasJustStarted) return false;

    boolean xFinished = false;
    switch (m_xMode) {
      case Constant:
        xFinished = true;
        break;
      case Control:
      case ControlCustom:
        xFinished = m_pidControllerX.atSetpoint();
        break;
      case ProfiledControl:
      case ProfiledControlCustom:
        xFinished = m_pidControllerProfiledX.atGoal();
        break;
    }
    boolean tFinished = false;
    switch (m_tMode) {
      case Constant:
        tFinished = true;
        break;
      case Control:
      case ControlCustom:
        tFinished = m_pidControllerProfiledTheta.atSetpoint();
        break;
      case ProfiledControl:
      case ProfiledControlCustom:
        tFinished = m_pidControllerProfiledTheta.atGoal();
        break;
    }
    return xFinished && tFinished;
  }
}
