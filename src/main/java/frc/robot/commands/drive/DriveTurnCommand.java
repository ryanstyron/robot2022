// Copyright (c) OpenRobotGroup.
// Open Source Software; you can modify and/or share it under the terms of
// the BSD license file in the root directory of this project.

package frc.robot.commands.drive;

import edu.wpi.first.wpilibj2.command.CommandBase;

import frc.robot.Constants.DrivetrainConstants;
import frc.robot.subsystems.DrivetrainSubsystem;

/**
 * Turns the robot to a set heading based on a given rotational speed.
 *
 * <p>Dependent on odometry, however does not implement advanced PID control.
 */
public class DriveTurnCommand extends CommandBase {

  // Subsystems
  private final DrivetrainSubsystem m_drivetrainSubsystem;

  // Proportion to set the drive motors.
  private double m_speed;

  // Degrees to turn the robot.
  private double m_rotation;

  // Whether the encoders indicate that the rotation has completed.
  private boolean m_hasTraveled = false;

  /**
   * Constructs a command that sets the rotational speed of the drivetrain to the given power until
   * a rotation in degrees is reached.
   *
   * @param drivetrainSubsystem The drivetrain subsystem.
   * @param speed Proportion to set the drive motors on the linear axis.
   * @param distance Rotation, in degrees, to turn the robot.
   */
  public DriveTurnCommand(DrivetrainSubsystem drivetrainSubsystem, double speed, double rotation) {
    m_drivetrainSubsystem = drivetrainSubsystem;
    m_speed = speed;
    m_rotation = rotation;

    addRequirements(m_drivetrainSubsystem);
  }

  /** Initializes the command. */
  @Override
  public void initialize() {
    // Reset the odometry.
    m_drivetrainSubsystem.resetOdometry(null);

    // To differentiate between clockwise and counterclockwise, the speed value must account for the
    // input's intention.
    m_speed = m_speed * Math.signum(m_rotation);

    // Set the boolean to true so the command can does not immediately terminate.
    m_hasTraveled = false;
  }

  /** Runs periodically while the command is scheduled. */
  @Override
  public void execute() {
    // Set the drive motors to the given proportion on the rotational axis.
    if (m_drivetrainSubsystem.getAbsoluteDistance()
        < Math.abs(m_rotation)
            / 360
            * Math.PI
            * DrivetrainConstants.kTrackWidth
            / DrivetrainConstants.kTurnError)
      m_drivetrainSubsystem.tankDrive(
          DrivetrainConstants.kMaxVolts * m_speed, -DrivetrainConstants.kMaxVolts * m_speed);
    else {
      m_drivetrainSubsystem.tankDrive(0.0, 0.0);
      m_hasTraveled = true;
    }
  }

  /** Runs when the command ends. */
  @Override
  public void end(boolean interrupted) {
    // Set the drive motors so that the robot is stationary.
    m_drivetrainSubsystem.tankDrive(0.0, 0.0);

    // Set the rotation as determined by the encoders.
    m_drivetrainSubsystem.setLastRotationalDistance(m_drivetrainSubsystem.getAbsoluteDistance());
  }

  /**
   * @return Whether the command has finished. Once a command finishes, the scheduler will call its
   *     end() method and un-schedule it.
   */
  @Override
  public boolean isFinished() {
    // End the command if the full rotation has occurred.
    return m_hasTraveled;
  }
}
