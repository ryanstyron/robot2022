// Copyright (c) OpenRobotGroup.
// Open Source Software; you can modify and/or share it under the terms of
// the BSD license file in the root directory of this project.

package frc.robot.commands.intake;

import edu.wpi.first.wpilibj2.command.CommandBase;

import frc.robot.subsystems.IntakeSubsystem;
import frc.robot.subsystems.IntakeSubsystem.IntakeSpeed;

/** Raises the intake until the motor has rotated such that the upper limit switch is pressed. */
public class IntakeRaiseCommand extends CommandBase {

  // Subsystems
  private final IntakeSubsystem m_intakeSubsystem;

  /** Constructs the command with the intake subsystem. */
  public IntakeRaiseCommand(IntakeSubsystem intakeSubsystem) {
    m_intakeSubsystem = intakeSubsystem;

    addRequirements(m_intakeSubsystem);
  }

  /** Runs periodically while the command is scheduled. */
  @Override
  public void execute() {
    // Set the intake motor to the raising speed.
    m_intakeSubsystem.setIntakeSpeed(IntakeSpeed.kRaise);
  }

  /** Runs when the command ends. */
  @Override
  public void end(boolean interrupted) {
    // Disable the intake motor when the command ends.
    m_intakeSubsystem.setIntakeSpeed(IntakeSpeed.kOff);
  }

  /**
   * @return Whether the command has finished. Once a command finishes, the scheduler will call its
   *     end() method and un-schedule it.
   */
  @Override
  public boolean isFinished() {
    // End the command if the upper intake limit switch has been pressed.
    return m_intakeSubsystem.isLimitSwitchUpperPressed();
  }
}
