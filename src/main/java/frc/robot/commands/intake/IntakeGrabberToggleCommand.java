// Copyright (c) OpenRobotGroup.
// Open Source Software; you can modify and/or share it under the terms of
// the BSD license file in the root directory of this project.

package frc.robot.commands.intake;

import edu.wpi.first.wpilibj2.command.CommandBase;

import frc.robot.subsystems.IntakeSubsystem;

/** Toggles the intake double solenoid to extend or retract the grabber. */
public class IntakeGrabberToggleCommand extends CommandBase {

  // Subsystems
  private IntakeSubsystem m_intakeSubsystem;

  // Whether or not the toggling has occurred.
  private boolean m_hasToggled = false;

  // Constructs the command with the intake subsystem.
  public IntakeGrabberToggleCommand(IntakeSubsystem intakeSubsystem) {
    m_intakeSubsystem = intakeSubsystem;

    addRequirements(m_intakeSubsystem);
  }

  /** Initializes the command. */
  @Override
  public void initialize() {
    // Ensure the boolean is set to false when initialized so the command does not immediately end.
    m_hasToggled = false;
  }

  /** Runs periodically while the command is scheduled. */
  @Override
  public void execute() {
    // Toggle the solenoid.
    m_intakeSubsystem.toggleGrabber();

    // Set the boolean to true to end the command.
    m_hasToggled = true;
  }

  /**
   * @return Whether the command has finished. Once a command finishes, the scheduler will call its
   *     end() method and un-schedule it.
   */
  @Override
  public boolean isFinished() {
    // End the command if the intake's grabber solenoid has toggled.
    return m_hasToggled;
  }
}
