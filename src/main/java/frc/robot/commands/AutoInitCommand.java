// Copyright (c) OpenRobotGroup.
// Open Source Software; you can modify and/or share it under the terms of
// the BSD license file in the root directory of this project.

package frc.robot.commands;

import edu.wpi.first.wpilibj.DoubleSolenoid;
import edu.wpi.first.wpilibj2.command.CommandBase;

import com.ctre.phoenix.motorcontrol.NeutralMode;

import frc.robot.Constants.HangerConstants;
import frc.robot.subsystems.DrivetrainSubsystem;
import frc.robot.subsystems.DrivetrainSubsystem.DriveSpeed;
import frc.robot.subsystems.HangerSubsystem;
import frc.robot.subsystems.IntakeSubsystem;
import frc.robot.subsystems.LauncherSubsystem;

/** Sets subsystems in intended states for autonomous mode. */
public class AutoInitCommand extends CommandBase {

  // Subsystems
  private final DrivetrainSubsystem m_drivetrainSubsystem;
  private final HangerSubsystem m_hangerSubsystem;
  private final IntakeSubsystem m_intakeSubsystem;
  private final LauncherSubsystem m_launcherSubsystem;

  // Whether the initialization has occurred.
  private boolean m_hasInitialized = false;

  /** Instantiates the drivetrain, intake, and launcher subsystems when creating the command. */
  public AutoInitCommand(
      DrivetrainSubsystem drivetrainSubsystem,
      HangerSubsystem hangerSubsystem,
      IntakeSubsystem intakeSubsystem,
      LauncherSubsystem launcherSubsystem) {
    m_drivetrainSubsystem = drivetrainSubsystem;
    m_hangerSubsystem = hangerSubsystem;
    m_intakeSubsystem = intakeSubsystem;
    m_launcherSubsystem = launcherSubsystem;

    addRequirements(drivetrainSubsystem, hangerSubsystem, intakeSubsystem, launcherSubsystem);
  }

  /** Initializes the command. */
  @Override
  public void initialize() {
    // Set the initialization to false to ensure the command is not returned as finished upon being
    // called for the first time.
    m_hasInitialized = false;
  }

  /** Runs periodically while the command is scheduled. */
  @Override
  public void execute() {
    // Set the neutral mode of the drive motors to break, to prevent coasting in autonomous.
    m_drivetrainSubsystem.setNeutralMode(NeutralMode.Brake);

    // Reset the drivetrain encoder values.
    m_drivetrainSubsystem.resetOdometry(null);

    // Allow usage of the drive motors' full voltage.
    m_drivetrainSubsystem.setMaxDriveSpeed(DriveSpeed.kMax);

    // Set the intake's grabber solenoid in the reverse state.
    m_intakeSubsystem.setGrabber(DoubleSolenoid.Value.kReverse);

    // Set the launcher solenoid in the reverse state.
    m_launcherSubsystem.setLauncher(DoubleSolenoid.Value.kReverse);

    // Set the hanger servos to be unlocked.
    m_hangerSubsystem.setLeftServo(HangerConstants.kServoLeftUnlocked);
    m_hangerSubsystem.setRightServo(HangerConstants.kServoRightUnlocked);

    // Set the boolean to true so that the command can terminate.
    m_hasInitialized = true;
  }

  /**
   * @return Whether the command has finished. Once a command finishes, the scheduler will call its
   *     end() method and un-schedule it.
   */
  @Override
  public boolean isFinished() {
    return m_hasInitialized;
  }
}
