// Copyright (c) OpenRobotGroup.
// Open Source Software; you can modify and/or share it under the terms of
// the BSD license file in the root directory of this project.

// Copyright (c) FIRST and other WPILib contributors.
// Open Source Software; you can modify and/or share it under the terms of
// the WPILib BSD license file in the root directory of this project.

package frc.robot;

import edu.wpi.first.math.util.Units;
import edu.wpi.first.wpilibj.smartdashboard.SendableChooser;
import edu.wpi.first.wpilibj2.command.Command;
import edu.wpi.first.wpilibj2.command.CommandGroupBase;
import edu.wpi.first.wpilibj2.command.InstantCommand;
import edu.wpi.first.wpilibj2.command.RunCommand;
import edu.wpi.first.wpilibj2.command.SequentialCommandGroup;
import edu.wpi.first.wpilibj2.command.WaitCommand;

import io.github.oblarg.oblog.annotations.Log;

import frc.robot.Constants.DriverStation;
import frc.robot.Constants.FieldConstants;
import frc.robot.Constants.HangerConstants;
import frc.robot.commands.AutoInitCommand;
import frc.robot.commands.DisabledInitCommand;
import frc.robot.commands.TeleopInitCommand;
import frc.robot.commands.drive.DriveLinearCommand;
import frc.robot.commands.drive.DriveTurnCommand;
import frc.robot.commands.hanger.HangerExtendCommand;
import frc.robot.commands.hanger.HangerRetractCommand;
import frc.robot.commands.intake.IntakeGrabberToggleCommand;
import frc.robot.commands.intake.IntakeLowerCommand;
import frc.robot.commands.intake.IntakeRaiseCommand;
import frc.robot.commands.launcher.LaunchCommand;
import frc.robot.commands.launcher.advanced.LaunchCargoCommand;
import frc.robot.input.F310Controller;
import frc.robot.input.F310Controller.Hand;
import frc.robot.subsystems.DrivetrainSubsystem;
import frc.robot.subsystems.DrivetrainSubsystem.DriveSpeed;
import frc.robot.subsystems.HangerSubsystem;
import frc.robot.subsystems.IntakeSubsystem;
import frc.robot.subsystems.IntakeSubsystem.IntakeSpeed;
import frc.robot.subsystems.LauncherSubsystem;
import frc.robot.subsystems.VisionSubsystem;

/**
 * This class is where the bulk of the robot should be declared. Since Command-based is a
 * "declarative" paradigm, very little robot logic should actually be handled in the {@link Robot}
 * periodic methods (other than the scheduler calls). Instead, the structure of the robot (including
 * subsystems, commands, and button mappings) should be declared here.
 */
public class RobotContainer {

  // Subsystems
  private final DrivetrainSubsystem m_drivetrainSubsystem = new DrivetrainSubsystem();
  private final HangerSubsystem m_hangerSubsystem = new HangerSubsystem();
  private final IntakeSubsystem m_intakeSubsystem = new IntakeSubsystem();
  private final LauncherSubsystem m_launcherSubsystem = new LauncherSubsystem();
  private final VisionSubsystem m_visionSubsystem = new VisionSubsystem();

  // Commands
  private final AutoInitCommand m_autoInitCommand =
      new AutoInitCommand(
          m_drivetrainSubsystem, m_hangerSubsystem, m_intakeSubsystem, m_launcherSubsystem);
  private final DisabledInitCommand m_disabledInitCommand =
      new DisabledInitCommand(m_drivetrainSubsystem, m_hangerSubsystem);
  private final TeleopInitCommand m_teleopInitCommand =
      new TeleopInitCommand(m_drivetrainSubsystem, m_hangerSubsystem);

  // Autonmous Chooser
  @Log(
      name = "Autonomous Chooser",
      width = 2,
      height = 1,
      rowIndex = 0,
      columnIndex = 0,
      tabName = "Driver View")
  private final SendableChooser<Command> m_autoChooser = new SendableChooser<>();

  // Controller Inputs
  private final F310Controller m_controllerDrive =
      new F310Controller(DriverStation.kPortControllerDrive);
  private final F310Controller m_controllerManip =
      new F310Controller(DriverStation.kPortControllerManip);

  /** The container for the robot. Contains subsystems, OI devices, and commands. */
  public RobotContainer() {

    // Configure Autonomous Selector
    /**
     * Default autonomous command that launches pre-loaded cargo, then proceeds to drive out of the
     * tarmac.
     */
    m_autoChooser.setDefaultOption(
        "Autonomous One-Cargo Attempt Non-Advanced Sequential",
        new SequentialCommandGroup(
            new LaunchCommand(m_launcherSubsystem),
            new DriveLinearCommand(
                m_drivetrainSubsystem,
                -DriveSpeed.kNormal.value,
                FieldConstants.kTarmacExitDistance)));
    /** Higher scoring autonomous. */
    m_autoChooser.addOption(
        "Autonomous Two-Cargo Attempt Non-Advanced Sequential",
        new SequentialCommandGroup(
            new LaunchCommand(m_launcherSubsystem),
            new DriveLinearCommand(
                m_drivetrainSubsystem, -DriveSpeed.kNormal.value, Units.inchesToMeters(42.0)),
            new IntakeGrabberToggleCommand(m_intakeSubsystem),
            new IntakeLowerCommand(m_intakeSubsystem),
            new IntakeGrabberToggleCommand(m_intakeSubsystem),
            new DriveTurnCommand(m_drivetrainSubsystem, DriveSpeed.kSlow.value, -90.0),
            new DriveLinearCommand(
                    m_drivetrainSubsystem, DriveSpeed.kSlow.value, Units.inchesToMeters(90))
                .withInterrupt(m_intakeSubsystem::hasGrabbed)
                .andThen(
                    () -> {
                      if (!m_intakeSubsystem.hasGrabbed()) m_intakeSubsystem.toggleGrabber();
                    }),
            new DriveLinearCommand(
                m_drivetrainSubsystem,
                -DriveSpeed.kSlow.value,
                m_drivetrainSubsystem.getLastLinearDistance()),
            new DriveTurnCommand(m_drivetrainSubsystem, DriveSpeed.kSlow.value, 90.0),
            new DriveLinearCommand(
                m_drivetrainSubsystem, DriveSpeed.kNormal.value, Units.inchesToMeters(90)),
            new IntakeRaiseCommand(m_intakeSubsystem),
            new IntakeGrabberToggleCommand(m_intakeSubsystem),
            new WaitCommand(1.0),
            new LaunchCommand(m_launcherSubsystem),
            new DriveLinearCommand(
                m_drivetrainSubsystem,
                -DriveSpeed.kNormal.value,
                FieldConstants.kTarmacExitDistance)));
    m_autoChooser.addOption(
        "Autonomous Turn (Test)", new DriveTurnCommand(m_drivetrainSubsystem, 0.25, -90.0));
    m_autoChooser.addOption("Autonomous Launch Cargo (Teleop Test)", new LaunchCargoCommand());

    /** Sets drivetrain to arcade mode, with speed and rotation determined by driver's input. */
    m_drivetrainSubsystem.setDefaultCommand(
        new RunCommand(
                () ->
                    m_drivetrainSubsystem.arcadeDrive(
                        m_controllerDrive.getIntensityY(Hand.kLeft),
                        m_controllerDrive.getIntensityX(Hand.kRight)),
                m_drivetrainSubsystem)
            .withName("Arcade Drive"));

    // Configure Buttons to Command Bindings
    configureButtonBindings();
  }

  /** Configures {@link F310Controller} button mappings. */
  private void configureButtonBindings() {

    // Autonomous teleoperated assist modes.
    m_controllerDrive.povDown.whenPressed(
        new LaunchCargoCommand().withInterrupt(m_controllerDrive.povRight::get));

    // Drivetrain speed modifiers.
    Command resetDriveSpeedCommand =
        new InstantCommand(
            () -> m_drivetrainSubsystem.setMaxDriveSpeed(DriveSpeed.kNormal),
            m_drivetrainSubsystem);
    m_controllerDrive
        .axisLt
        .whenPressed(
            () -> m_drivetrainSubsystem.setMaxDriveSpeed(DriveSpeed.kSlow), m_drivetrainSubsystem)
        .whenReleased(resetDriveSpeedCommand);
    m_controllerDrive
        .axisRt
        .whenPressed(
            () -> m_drivetrainSubsystem.setMaxDriveSpeed(DriveSpeed.kFast), m_drivetrainSubsystem)
        .whenReleased(resetDriveSpeedCommand);

    // Intake extension and lifting.
    Command resetIntakeSpeedCommand =
        new InstantCommand(
            () -> m_intakeSubsystem.setIntakeSpeed(IntakeSpeed.kOff), m_intakeSubsystem);
    m_controllerManip
        .buttonB
        .whileHeld(() -> m_intakeSubsystem.setIntakeSpeed(IntakeSpeed.kRaise), m_intakeSubsystem)
        .whenReleased(resetIntakeSpeedCommand);
    m_controllerManip
        .buttonX
        .whileHeld(() -> m_intakeSubsystem.setIntakeSpeed(IntakeSpeed.kLower), m_intakeSubsystem)
        .whenReleased(resetIntakeSpeedCommand);
    m_controllerManip.buttonY.whenPressed(
        () -> m_intakeSubsystem.toggleGrabber(), m_intakeSubsystem);

    // Launching solenoid toggle.
    m_controllerManip.buttonA.whenPressed(new LaunchCommand(m_launcherSubsystem));

    // Hanging controls.
    m_controllerDrive.buttonA.whenPressed(
        new HangerExtendCommand(m_hangerSubsystem, HangerConstants.kSpeedExtend));
    m_controllerDrive.buttonB.whenHeld(
        new HangerRetractCommand(m_hangerSubsystem, HangerConstants.kSpeedRetract));
    m_controllerDrive.buttonStart.whenPressed(
        new InstantCommand(
            () -> {
              m_hangerSubsystem.setLeftServo(HangerConstants.kServoLeftLocked);
              m_hangerSubsystem.setRightServo(HangerConstants.kServoRightLocked);
            },
            m_hangerSubsystem));
    m_controllerDrive.buttonBack.whenPressed(
        new InstantCommand(
            () -> {
              m_hangerSubsystem.setLeftServo(HangerConstants.kServoLeftUnlocked);
              m_hangerSubsystem.setRightServo(HangerConstants.kServoRightUnlocked);
            },
            m_hangerSubsystem));
  }

  /**
   * Passes the autonomous command to the main {@link Robot} class.
   *
   * @return The command to run in autonomous.
   */
  public Command getAutoInitCommand() {
    // Set the autonomous command to that selected in the {@link SendableChooser}.
    Command autoCommand = m_autoChooser.getSelected();

    // Clear stashed autonmous commands.
    CommandGroupBase.clearGroupedCommand(m_autoInitCommand);
    CommandGroupBase.clearGroupedCommand(autoCommand);

    // Initialize values prior to executing the intended command.
    return m_autoInitCommand.andThen(autoCommand);
  }

  /**
   * Passes the disabled init command to the main {@link Robot} class.
   *
   * @return The command to run when the robot is disabled.
   */
  public Command getDisabledInitCommand() {
    return m_disabledInitCommand;
  }

  /**
   * Passes the teleop init command to the main {@link Robot} class.
   *
   * @return The command to run in teleoperated mode.
   */
  public Command getTeleopInitCommand() {
    return m_teleopInitCommand;
  }
}
