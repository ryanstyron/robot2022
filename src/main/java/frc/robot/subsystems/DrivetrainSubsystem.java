// Copyright (c) OpenRobotGroup.
// Open Source Software; you can modify and/or share it under the terms of
// the BSD license file in the root directory of this project.

package frc.robot.subsystems;

import edu.wpi.first.math.geometry.Pose2d;
import edu.wpi.first.math.kinematics.DifferentialDriveOdometry;
import edu.wpi.first.math.kinematics.DifferentialDriveWheelSpeeds;
import edu.wpi.first.math.util.Units;
import edu.wpi.first.wpilibj.ADXRS450_Gyro;
import edu.wpi.first.wpilibj.CounterBase.EncodingType;
import edu.wpi.first.wpilibj.Encoder;
import edu.wpi.first.wpilibj.drive.DifferentialDrive;
import edu.wpi.first.wpilibj.smartdashboard.Field2d;
import edu.wpi.first.wpilibj.smartdashboard.SmartDashboard;
import edu.wpi.first.wpilibj2.command.SubsystemBase;

import io.github.oblarg.oblog.Loggable;
import io.github.oblarg.oblog.annotations.Log;

import com.ctre.phoenix.motorcontrol.NeutralMode;
import com.ctre.phoenix.motorcontrol.can.WPI_TalonSRX;

import frc.robot.Constants.DrivetrainConstants;
import frc.robot.Constants.DrivetrainConstants.EncoderConstants;
import frc.robot.Constants.RoboRIO;
import frc.robot.Constants.RoboRIO.CAN;
import frc.robot.Utils;

/**
 * Represents the drivetrain subsystem.
 *
 * <p>The drivetrain encompasses the wheels of the robot, encoders that measure their movement, and
 * motors that provide power.
 */
public class DrivetrainSubsystem extends SubsystemBase implements Loggable {

  /** Represents values to which the drive motors will be set. */
  public enum DriveSpeed {
    kSlow(DrivetrainConstants.kSpeedSlow),
    kNormal(DrivetrainConstants.kSpeedNormal),
    kFast(DrivetrainConstants.kSpeedFast),
    kMax(1.0);

    public final double value;

    DriveSpeed(double value) {
      this.value = value;
    }
  }

  // Motor Controllers
  private final WPI_TalonSRX m_motorFrontLeft = new WPI_TalonSRX(CAN.kPortMotorDriveFrontLeft);
  private final WPI_TalonSRX m_motorFrontRight = new WPI_TalonSRX(CAN.kPortMotorDriveFrontRight);
  private final WPI_TalonSRX m_motorBackLeft = new WPI_TalonSRX(CAN.kPortMotorDriveBackLeft);
  private final WPI_TalonSRX m_motorBackRight = new WPI_TalonSRX(CAN.kPortMotorDriveBackRight);

  // Quadrature Encoders
  @Log(
      name = "Drive Left Encoder (Relative)",
      width = 2,
      height = 1,
      rowIndex = 0,
      columnIndex = 3,
      tabName = "Driver View")
  private final Encoder m_encoderLeft =
      new Encoder(
          RoboRIO.DIO.kPortEncoderDriveLeft[0],
          RoboRIO.DIO.kPortEncoderDriveLeft[1],
          false,
          EncodingType.k1X);

  @Log(
      name = "Drive Right Encoder (Relative)",
      width = 2,
      height = 1,
      rowIndex = 1,
      columnIndex = 3,
      tabName = "Driver View")
  // The right encoder is reversed to account for the negation of the right motors.
  private final Encoder m_encoderRight =
      new Encoder(
          RoboRIO.DIO.kPortEncoderDriveRight[0],
          RoboRIO.DIO.kPortEncoderDriveRight[1],
          true,
          EncodingType.k1X);

  // Gyroscope
  @Log(
      name = "Gyroscope",
      width = 2,
      height = 3,
      rowIndex = 0,
      columnIndex = 6,
      tabName = "Driver View")
  private final ADXRS450_Gyro m_gyro = new ADXRS450_Gyro();

  // Differential Drive
  private final DifferentialDrive m_differentialDrive =
      new DifferentialDrive(m_motorFrontLeft, m_motorFrontRight);
  private final DifferentialDriveOdometry m_odometry =
      new DifferentialDriveOdometry(m_gyro.getRotation2d());

  // Field Representation
  private Field2d m_field = new Field2d();

  // Distances determined by encoders for dead reckoning purposes.
  private double m_lastLinearDistance = 0.0;
  private double m_lastRotationalDistance = 0.0;

  /** Initializes the drivetrain subsystem. */
  public DrivetrainSubsystem() {
    // Revert all motor controller configurations to their factory default values.
    m_motorFrontLeft.configFactoryDefault();
    m_motorFrontRight.configFactoryDefault();
    m_motorBackLeft.configFactoryDefault();
    m_motorBackRight.configFactoryDefault();

    // Invert the right side motors to account for their default negation.
    m_motorFrontRight.setInverted(true);
    m_motorBackRight.setInverted(true);

    // Set the back motors to follow their front side counterparts.
    m_motorBackLeft.follow(m_motorFrontLeft);
    m_motorBackRight.follow(m_motorFrontRight);

    // Configure the encoders.
    m_encoderLeft.setSamplesToAverage(EncoderConstants.kEncoderSamplesToAverage);
    m_encoderLeft.setDistancePerPulse(EncoderConstants.kEncoderDistancePerPulse);
    m_encoderRight.setSamplesToAverage(EncoderConstants.kEncoderSamplesToAverage);
    m_encoderRight.setDistancePerPulse(EncoderConstants.kEncoderDistancePerPulse);

    // Reset the robot odometry.
    resetOdometry(null);

    // Pass field visualization through SmartDashboard.
    SmartDashboard.putData("Field", m_field);
  }

  /** This method is run periodically. */
  @Override
  public void periodic() {
    // Update the odometry.
    m_odometry.update(
        m_gyro.getRotation2d(), m_encoderLeft.getDistance(), m_encoderRight.getDistance());

    // Update the field.
    m_field.setRobotPose(m_odometry.getPoseMeters());
  }

  /**
   * Sets the neutral mode of the drive motors.
   *
   * @param mode The neutral mode.
   */
  public void setNeutralMode(NeutralMode mode) {
    m_motorFrontLeft.setNeutralMode(mode);
    m_motorFrontRight.setNeutralMode(mode);
    m_motorBackLeft.setNeutralMode(mode);
    m_motorBackRight.setNeutralMode(mode);
  }

  /**
   * Sets the maximum speed the robot will drive.
   *
   * @param speed {@link DriveSpeed} constant to which the max motor output will be set.
   */
  public void setMaxDriveSpeed(DriveSpeed speed) {
    m_differentialDrive.setMaxOutput(speed.value);
  }

  /**
   * Drives the robot using proportions of max motor output.
   *
   * @param xSpeed Speed along the X axis [-1.0..1.0], where forward is positive.
   * @param zRotation Rotation rate around the Z axis [-1.0..1.0], where clockwise is positive.
   */
  public void arcadeDrive(double xSpeed, double zRotation) {
    // The slew function is applied to the inputs so that driving seems more fluid to
    // the driver.
    m_differentialDrive.arcadeDrive(Utils.slew(xSpeed), Utils.slew(zRotation));
  }

  /**
   * Drives the robot using voltages.
   *
   * @param leftVoltage Left motor output.
   * @param rightVoltage Right motor output.
   */
  public void tankDrive(double leftVoltage, double rightVoltage) {
    m_motorFrontLeft.setVoltage(leftVoltage);
    m_motorFrontRight.setVoltage(rightVoltage);
    m_differentialDrive.feed();
  }

  /**
   * Resets the sensors and pose used for odometry. The gyro is reset to a heading of zero, and the
   * encoder counts are set to zero.
   *
   * @param pose The relative position on the field that the robot is at.
   */
  public void resetOdometry(Pose2d pose) {
    // If no pose is instantiated, create a new object with the default (0, 0) translation.
    if (pose == null) pose = new Pose2d();

    // Reset the encoders.
    m_encoderLeft.reset();
    m_encoderRight.reset();

    // Reset the gyroscope.
    m_gyro.reset();

    // Reset the odometry.
    m_odometry.resetPosition(pose, m_gyro.getRotation2d());
  }

  /** @return The current wheel speeds of the robot. */
  public DifferentialDriveWheelSpeeds getWheelSpeeds() {
    return new DifferentialDriveWheelSpeeds(m_encoderLeft.getRate(), m_encoderRight.getRate());
  }

  /** @return The estimated pose of the robot. */
  public Pose2d getPose() {
    return m_odometry.getPoseMeters();
  }

  /**
   * Sets the last linear distance to a given value.
   *
   * @param lastLinearDistance Distance, in meters, on the previous linear drive.
   */
  public void setLastLinearDistance(double lastLinearDistance) {
    m_lastLinearDistance = lastLinearDistance;
  }

  /**
   * Sets the last rotational distance to a given value.
   *
   * @param lastLinearDistance Distance, in meters, on the previous rotational drive.
   */
  public void setLastRotationalDistance(double lastRotationalDistance) {
    m_lastRotationalDistance = lastRotationalDistance;
  }

  /** @return The set value, in meters, for the last linear drive. */
  public double getLastLinearDistance() {
    return m_lastLinearDistance;
  }

  /** @return The set value, in meters, for the last rotational drive. */
  public double getLastRotationalDistance() {
    return m_lastRotationalDistance;
  }

  /** @return The average distance traveled as determined by the two drive encoders. */
  public double getDistance() {
    return (m_encoderLeft.getDistance() + m_encoderRight.getDistance()) / 2.0;
  }

  /**
   * @return The average distance traveled as determined by the absolute values of the two drive
   *     encoders.
   */
  public double getAbsoluteDistance() {
    return (Math.abs(m_encoderLeft.getDistance()) + Math.abs(m_encoderRight.getDistance())) / 2.0;
  }

  /** @return The robot's heading in degrees, from -pi to pi. */
  public double getHeading() {
    return m_gyro.getRotation2d().getRadians();
  }

  /** @return The average rate of change in distance between the two drive encoders. */
  public double getSpeed() {
    return (m_encoderLeft.getRate() + m_encoderRight.getRate()) / 2.0;
  }

  /** @return The rate of change in the gyroscope. */
  public double getTurnRate() {
    return Units.degreesToRadians(m_gyro.getRate());
  }
}
