// Copyright (c) OpenRobotGroup.
// Open Source Software; you can modify and/or share it under the terms of
// the BSD license file in the root directory of this project.

package frc.robot.subsystems;

import edu.wpi.first.wpilibj.DoubleSolenoid;
import edu.wpi.first.wpilibj.PneumaticsModuleType;
import edu.wpi.first.wpilibj2.command.SubsystemBase;

import frc.robot.Constants.RoboRIO.CAN;

/**
 * Represents the launcher subsystem.
 *
 * <p>Encompasses a pneumatic double solenoid that catapults cargo.
 */
public class LauncherSubsystem extends SubsystemBase {

  // Solenoids
  private final DoubleSolenoid m_doubleSolenoidLauncher =
      new DoubleSolenoid(
          PneumaticsModuleType.CTREPCM,
          CAN.kPortDoubleSolenoidLauncher[0],
          CAN.kPortDoubleSolenoidLauncher[1]);

  /** @return The state of the launcher's solenoid. */
  public DoubleSolenoid.Value getLauncher() {
    return m_doubleSolenoidLauncher.get();
  }

  /**
   * Sets the launcher solenoid in the specified state.
   *
   * @param value Value to set the launcher solenoid.
   */
  public void setLauncher(DoubleSolenoid.Value value) {
    m_doubleSolenoidLauncher.set(value);
  }

  /** Toggles the solenoid to raise or retract the launcher. */
  public void toggleLauncher() {
    if (getLauncher() == DoubleSolenoid.Value.kOff) setLauncher(DoubleSolenoid.Value.kReverse);
    m_doubleSolenoidLauncher.toggle();
  }
}
